<?php
/**
 * @file
 * Views Pager plugin definition
 */

/**
 * Implementation of hook_views_plugins
 */
function views_calcpager_views_plugins() {
  $plugins = array(
    'pager' => array(
      'calc' => array(
        'title' => t('Paged output, calc pager'),
        'help' => t('Use the calc pager output.'),
        'handler' => 'views_calcpager_plugin_pager_calc',
        'help topic' => 'pager-calc',
        'uses options' => TRUE,
        'parent' => 'full',
      ),
    ),
  );

  return $plugins;
}

