
Module: Views Calcpager
Author: Peter J. Drake <pdrake@gmail.com>

Description
===========
The Views Calcpager module solves a problem of scalability for sites with large
amounts of content. COUNT queries executed on large datasets when using the
InnoDB MySQL engine can be painfully slow. This module sends the 
SQL_CALC_FOUND_ROWS SQL hint to MySQL and then uses the found_rows() function to
determine the total number of rows.  This pager should only be used with views
backed by MySQL and should be tested with each view to determine if it provides
a performance improvement or detriment.

Installation
============

1. Download the module, unzip the source, and put the resulting directory into the 
   modules/ directory of your Drupal application.
2. Enable the module in Drupal at Administer > Site Building > Modules > Views

Usage
=====

To use the Calc pager for a specific view, navigate to your view's edit interface
and click to edit the pager settings. You will see a new option called Calc pager.
Simply select this Calc pager option and save your view.
